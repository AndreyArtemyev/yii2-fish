#!/bin/sh
container_prefix="project-backend"

DB_COMMAND=$(cat <<-END
    mysql -hlocalhost -uroot -p123 -e "
    CREATE DATABASE IF NOT EXISTS project;
    use project;";
END
)

docker exec $container_prefix"-mysql" sh -c "$DB_COMMAND";
