#!/bin/sh
# Запускает миграции Yii в контейнере

container_prefix="project-backend"

docker exec $container_prefix"-fpm" bash -c "php yii migrate"
